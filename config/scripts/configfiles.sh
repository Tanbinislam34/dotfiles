#!/usr/bin/env bash
# _____ ___
#|_   _|_ _|
#  | |  | |
#  | |  | |
#  |_| |___| configfiles.sh 

# configfile.sh script for opening config files in set terminal or text editor(not emacs, because emacs is NOT a text editor)

declare -A config

#editor="emacsclient -c -a emacs"
editor="st -e nvim"

config[alacritty.yml]="$HOME/.config/alacritty/alacritty.yml"
config[alacritty[colors]]="$HOME/.config/alacritty/colors.yml"
config[alacritty.yml[fonts]]="$HOME/.config/alacritty/fonts.yml"
config[vis config]="$HOME/.config/vis/config"
config[bspconfig]="$HOME/.config/bspwm/bspwmrc"
config[keybinds]="$HOME/.config/sxhkd/sxhkdrc"
config[polybar config]="$HOME/.config/polybar/config"
config[st config]="$HOME/.config/st/config.h"
config[Xresources]="$HOME/.Xresources"
config[nvim init.el]="$HOME/.config/nvim/init.vim"
config[doom config.el]="$HOME/.doom.d/config.el"
config[qutebrowser]="$HOME/.config/qutebrowser/config.py"
config[ranger config]="$HOME/.config/ranger/rc.conf"
config[picom config]="$HOME/.config/picom/picom.conf"
config[neofetch config]="$HOME/.config/neofetch/config.conf"
config[firefox userchrome.css]="$HOME/.mozilla/firefox/pw6utfwm.default-release/chrome/userChrome.css"
config[configfiles.sh script]="$HOME/.scripts/configfiles.sh"

title=$(printf '%s\n' "${!config[@]}" | sort |  dmenu  -p "EDIT " -sb "#7aa2f7" "$@")

if [ "$title" ]; then
  url="${config["${title}"]}"
  $editor "${url}"
else
  echo "Program terminated." && exit 0
fi

