#!/usr/bin/env bash

wall=$(/bin/ls -al ~/.files/wallpapers/ | awk '{print $9}' | shuf -n 1)
feh --bg-fill --no-fehbg ~/.files/wallpapers/$wall


