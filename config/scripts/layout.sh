#!/usr/bin/env bash

## sets bspwm master stack layout

bsp-layout set tall I --master-size 0.5
bsp-layout set tall II --master-size 0.5
bsp-layout set tall III --master-size 0.5
bsp-layout set tall IV --master-size 0.5
bsp-layout set tall V --master-size 0.5
bsp-layout set tall VI --master-size 0.5
bsp-layout set tall VII --master-size 0.5
bsp-layout set tall VIII --master-size 0.5
bsp-layout set tall IX --master-size 0.5
bsp-layout set tall X --master-size 0.5

