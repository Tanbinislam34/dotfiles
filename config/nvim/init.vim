" _____ ___
"|_   _|_ _|
"  | |  | |
"  | |  | |
"  |_| |___| neovim init.el
"
"


" Setting Plugins

call plug#begin('~/.config/nvim/plugged')

Plug 'ghifarit53/tokyonight-vim'
Plug 'morhetz/gruvbox'
Plug 'joshdick/onedark.vim'
Plug 'scrooloose/nerdtree'
Plug 'nekonako/xresources-nvim'
Plug 'ajmwagar/vim-deus'
"Plug 'vim-airline/vim-airline'
"Plug 'vim-airline/vim-airline-themes'
Plug 'itchyny/lightline.vim'
Plug 'chrisbra/Colorizer'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'flazz/vim-colorschemes'
Plug 'godlygeek/tabular'
Plug 'preservim/vim-markdown'
Plug 'jceb/vim-orgmode'
Plug 'ryanoasis/vim-devicons'
Plug 'mhinz/vim-startify'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'dracula/vim'
Plug 'tyrannicaltoucan/vim-deep-space'
Plug 'drewtempelmeyer/palenight.vim'

" Initialize plugin system

call plug#end()

" End of plugins

" setting NeoVim Transparency
set termguicolors
let g:tokyonight_transparent_background = 1
colorscheme tokyonight
let g:tokyonight_style = 'night'
let t:is_transparent = 0
function! Toggle_transparent()
    if t:is_transparent == 0
        hi Normal guibg=NONE ctermbg=NONE
        let t:is_transparent = 1
    else
        set background=dark
        let t:is_tranparent = 0
    endif
endfunction
nnoremap <F1> : call Toggle_transparent()<CR>

let g:lightline = {
	\ 'colorscheme': 'tokyonight',
        \ 'mode_map': {
        \ 'n' : 'N',
        \ 'i' : 'I',
        \ 'R' : 'R',
        \ 'v' : 'V',
        \ 'V' : 'VL',
        \ "\<C-v>": 'VB',
        \ 'c' : 'C',
        \ 's' : 'S',
        \ 'S' : 'SL',
        \ "\<C-s>": 'SB',
        \ 't': 'T',
        \ },
      \ }

" setting color schemes

"highlight Normal ctermbg=NONE guibg=NONE
"highlight NonText ctermbg=NONE guibg=NONE
highlight StatusLine guibg=256 guifg=256
let g:airline_theme="tokyonight"
let g:airline_powerline_fonts = 1
"let g:airline#extensions#tabline#enabled = 1
set number
set relativenumber
set title
set noruler
" NerdTree toggle keybinding
nnoremap <F4> :NERDTreeToggle<CR>
let NERDTreeShowHidden=1
set noshowmode

" startify custom header
let g:startify_custom_header = [ ]

set wildmode=longest,list,full

" Run xrdb whenever Xdefaults or Xresources are updated.
	autocmd BufRead,BufNewFile Xresources,Xdefaults,xresources,xdefaults set filetype=xdefaults
	autocmd BufWritePost Xresources,Xdefaults,xresources,xdefaults !xrdb %



