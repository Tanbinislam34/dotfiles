# dotfiles
my personal dotfiles used for my Linux setup

![BSPWM rice](bspwm.png)

## rice information

- OS - I dont have a Specific Linux distro (i like arch based distros tho)
- WM - BSPWM
- Colorscheme - Modified version of Dracula 
- Terminal - Both st and Alacritty
- wallpaper - in wallpapers/30.jpg (removed) 
- Fetch Program - Neofetch
- Text editor - Neovim
- Browser - Firefox With Tabliss

